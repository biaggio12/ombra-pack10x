local config = {
    time = 1,
    storage = 200011
}
 
function onUse(player, item, fromPosition, target, toPosition, isHotkey)
    local playerPos = player:getPosition()
    local chance = math.random(1, 2)
 
    if player:getStorageValue(config.storage) >= os.time() then
        return player:sendTextMessage(MESSAGE_INFO_DESCR, "You need to wait 1 more hour before using it.")
    end
 
    local amulet = player:getSlotItem(CONST_SLOT_NECKLACE)
    if amulet and amulet.itemid == 25423 then
        if chance == 1 then
            player:addHealth(1000)
            playerPos:sendMagicEffect(CONST_ME_MAGIC_RED)
        else
            player:addMana(1000)
            playerPos:sendMagicEffect(CONST_ME_MAGIC_BLUE)
        end
           player:setStorageValue(config.storage, os.time() + 60 * 60)
    end
    return false
end